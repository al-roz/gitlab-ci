package com.example.task11;

import org.assertj.core.internal.BigDecimals;

import java.math.BigDecimal;

public class Task11
{

    public static float benefit(float sum, float percent)
    {
        // TODO исправьте функцию, чтобы избежать накопления ошибки
        // Считаем проценты за год
        sum = (float) ((double) (sum) * StrictMath.pow(1d + percent, 12d));
        return sum;
    }

    public static void main(String[] args)
    {

        float sum = 500; // 500 руб. на счете
        float percent = 0.00000001f; // 0.000001% ежемесячно


        sum = benefit(sum, percent);

        System.out.println("Сумма на счете через год: " + sum);

    }

}
