package com.example.task02;

public class Task02 {

    public static String solution(String input) {

        // TODO напишите здесь свою корректную реализацию этого метода, вместо сеществующей
        long inputNumber = Long.parseLong(input);

        if (Byte.MIN_VALUE <= inputNumber && inputNumber <= Byte.MAX_VALUE)
        {
            return Byte.TYPE.getName();
        }

        if (Short.MIN_VALUE <= inputNumber && inputNumber <= Short.MAX_VALUE)
        {
            return Short.TYPE.getName();
        }

        if (Integer.MIN_VALUE <= inputNumber && inputNumber <= Integer.MAX_VALUE)
        {
            return Integer.TYPE.getName();
        }


        return Long.TYPE.getName();
    }

    public static void main(String[] args) {
        // Здесь вы можете вручную протестировать ваше решение, вызывая реализуемый метод и смотря результат
        // например вот так:

        String result = solution("12345");
        System.out.println(result);
    }
}

